# LEIA-ME #

Esclarecimentos:
- Não foi utilizado controle de transação, pois todas operações são simples e o framework controla muito bem todas elas.
- O Cache com Redis trouxe uma melhora de 90 a 100 ms (teste local).
- As views foram feitas utilizando ViewSets + router (maior nível de abstração) (branch master) e da maneira convencional (branch feature/views).
- Foi criada uma classe de permissoes para controlar os métodos de requisição HTTP.


##Instruções:##
- Tour-points préviamente cadastrados: tour_points.json
- Instalação local: requer Redis e PostgreSQL.
- Instalação servidor: requer docker e docker-compose, utilizar o comando:
```
#!bash

docker-compose -f docker-compose.yml up
```

## API's: ##

- Listar tour-points (usuários anônimos):  somente tours-points das categorias com flag private = False.
- Listar tour-points (usuários logados): todas categorias, todos tour-points com flag private = False.
- Criar tour-point (usuários logados): qualquer usuário logado.
- Deletar tour-point (usuários logados): valida se usuário é dono do tour-point.
- Listar meus tour-points (usuários logados): lista apenas tour-points do próprio usuário.


##Postman:##
- postman_collection.json

##Backend:##
- ViewSet's (branch master)
- Views comuns (branch feature/views)
- Redis (cache de todas tabelas)
- PostgreSQL (Extension Postgis)

##Módulos:##
- GeoDjango (geolocalização)
- Django rest-framework Social Oauth2
- Django-cacheops
- Redis

##Docker##
- Docker-compose
- Gunicorn
- nginx
- PostgreSql (Extension Postgis)
- Redis