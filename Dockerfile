FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV REDIS_HOST "redis"
RUN apt-get -y -q update && apt-get install -y -q \
    libgdal-dev \
    libevent-dev \
    supervisor \
    nginx
RUN apt-get clean
# NGINX
ADD .docker/nginx-tourpoint /etc/nginx/sites-available/nginx-tourpoint
ADD .docker/nginx-tourpoint /etc/nginx/sites-enabled/nginx-tourpoint
RUN rm /etc/nginx/sites-enabled/default
RUN rm /etc/nginx/sites-available/default
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
# SUPERVISORD
ADD .docker/supervisord.conf /etc/supervisord.conf
ADD .docker/run.sh /usr/local/bin/run

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install -r requirements.txt

#RUN python manage.py migrate
#CMD ["/bin/sh", "-e", "/usr/local/bin/run"]