from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D, Distance

from django.db.models import Q
from rest_framework import generics
from rest_framework import permissions
from rest_framework import viewsets

from apps.tour_points.models import TourPoint, TourCategory
from apps.tour_points.permissions import IsOwnerOrReadOnly
from apps.tour_points.serializers import TourPointSerializer


class TourPointViewSet(viewsets.ModelViewSet):
    """ View que permite a listagem e criação de tourpoints
    Attributes:
        permission_classes Define a classe de permissões
        serializer_class   Define a classe serializadora para os tourpoints
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)
    serializer_class = TourPointSerializer
    """
        Método sobrescrito para atribuir usuário como dono do tourpoint
    """
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    """ Filtro para listar tourpoints
        Exclui Categorias com flag private para usuários anônimos
        Exclui Tour Points com flag private para Pesquisa geral Exceto para o dono do tour point
    """
    def get_queryset(self):
        if self.request.user.is_anonymous.value:
            private_categories = TourCategory.objects.filter(private=True)
            queryset = TourPoint.objects.all().exclude(category__in=private_categories)
        else:
            queryset = TourPoint.objects.filter(Q(private=False) | Q(owner=self.request.user))
        try:
            # Filtra tour-points com raio especificado na url
            distance_km = float(self.request.query_params['distance_km'])
            lat = float(self.request.query_params['lat'])
            lon = float(self.request.query_params['lon'])
            geolocation = Point(lon, lat)
            return queryset.filter(location__distance_lte=(geolocation, Distance(km=distance_km))).distance(
                geolocation).order_by('distance')
        except KeyError:
            return queryset


class TourPointMeViewSet(viewsets.ModelViewSet):
    """ View utilizada para listar meus tourpoints
    Attributes:
        permission_classes Define a classe de permissões
        queryset           Define a query base para pesquisa
        serializer_class   Define a classe serializadora para os tourpoints
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = TourPointSerializer
    """
        Método sobrescrito para definir query (Retorna apenas meus tourpoints)
    """
    def get_queryset(self):
        return TourPoint.objects.filter(owner=self.request.user)
