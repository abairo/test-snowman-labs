from drf_extra_fields.geo_fields import PointField
from rest_framework import serializers

from apps.tour_points.models import TourCategory, TourPoint


class TourCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TourCategory
        fields = '__all__'


class TourPointSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    location = PointField()

    class Meta:
        model = TourPoint
        fields = '__all__'
