"""test_snowmanlabs URL Configuration
    Tour Points
"""
from django.conf.urls import url
from rest_framework.compat import include
from rest_framework.routers import DefaultRouter

from apps.tour_points.views import TourPointViewSet, TourPointMeViewSet

router = DefaultRouter()
router.register(r'me', TourPointMeViewSet, base_name='tour-points-me')
router.register(r'', TourPointViewSet, base_name='tour-points')

urlpatterns = [
    url(r'^', include(router.urls)),
]