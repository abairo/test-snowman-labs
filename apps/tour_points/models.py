from django.contrib.gis.db import models


class TourCategory(models.Model):
    """ Modelo para definir as categorias possiveis de um tourpoint

    Attributes:
        name    Define o nome da categoria, permite até 100 caracteres
        private Define a visibilidade de uma categoria (Categorias públicas podem ser vistas
                por usuários anônimos)
    """
    name = models.CharField(null=False, blank=False, max_length=100)
    private = models.BooleanField(null=False, default=True)

    def __str__(self):
        return self.name


class TourPoint(models.Model):
    """ Modelo para definir TourPoints

       Attributes:
           owner    Define o proprietário (Auth.User) de um tourpoint
           name     Define o nome de um toupoint (max 100 char)
           category Define a categoria de um tourpoint
           location Define a geolocalização de um tourpoint
           objects  atributo especial utilizado em consultas específicas envolvendo geolocalização
           private  Define a visibiliade de um tourpoint
    """
    owner = models.ForeignKey('auth.User', related_name='tour_points', on_delete=models.CASCADE)
    name = models.CharField(null=False, blank=False, max_length=100)
    category = models.ForeignKey(TourCategory, on_delete=models.CASCADE, null=False, blank=False, related_name='tour_points')
    location = models.PointField(blank=True, null=True)
    objects = models.GeoManager()
    private = models.BooleanField(null=False, default=False)

    def __str__(self):
        return self.name
