from django.contrib import admin

from apps.tour_points.models import TourPoint, TourCategory

admin.site.register(TourPoint)
admin.site.register(TourCategory)
