from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
        Classe customizada de permissão:
            - Usuário anônimo: pode somente listar tourpoints referentes a Restaurantes
            - Usuários logados: podem somente listar tourpoints referentes a Restaurantes e públicos
            - Usuários logados: podem somemente excluir seus próprios tourpoints
    """
    def has_object_permission(self, request, view, obj):
        # Permite apenas métodos seguros como: GET, HEAD, OPTIONS
        if request.method in permissions.SAFE_METHODS:
            return True

        # Permissão total para Usuário logado e dono do tourpoint
        return obj.owner == request.user
