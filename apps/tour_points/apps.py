from django.apps import AppConfig


class TourPointsConfig(AppConfig):
    name = 'tour_points'
