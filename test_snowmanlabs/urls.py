"""test_snowmanlabs URL Configuration

    Admin - Panel
    Authentication - Oauth2 - Social Oauth2
    Tour Points
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'tour-points/', include('apps.tour_points.urls')),
]
